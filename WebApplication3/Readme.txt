﻿Datepickeri lisamiseks - mis ma tegin

1. Employee mudelis lisasin HireDate ja BirthDate väljadele
	data annotatsioonid (nurksulgudes osad seal ees)

2. Employee Edit view peale panin nendele kahele väljale lisaks datepicker klassi

3. _Layout vaatele lisasin paar scripti
	datepickeri initsialiseerimiseks read 45-54
	eestikeelse toe llisamiseks read 55-97

4. Otsisin ja paigaldasin oma rakendusele datepickeri moodulid
	Leidsin: http://jqueryui.com/download/#!version=1.10.4

	laadisin alla ja paigutasin kausta jquery-ui oma contenti alla

5. Lisasin klassi

	DateTimeModelBinder

6. Lisasin kaks rida _Layout viewsse

	Rida 8 ja rida rida 41

	PEALE NEID ASJU HAKKAS MUL DATEPICKER ilusti tööle

LISAN KA Web Api Controlleri 

1. Mudelisse panin PRodutid lisaks eelnevale
	NB! mudeli klassid vaja üle käia ja data annotatsioonid tagasi lisada

2. Lisan uue kontrolleri Web Api 2.0 Entity ja actionitega

Paar asja vaja teha, et see toimima hakkaks brouseri aknas normaalselt
a) lisasin Data Context (NOrthwind Entities) klassi konstruktorisse paar rida:

            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;

b) lisasin App_Start forlderisse ühe klassi
		WebApiConfig.cs (sisu suht lihtne)
		Kui alustada Web Api rakendusega, siis on see automaatselt olemas
		Lisasin vastava rea (vt.) Global.asax faili

c) Global.asax
		sinna veel pisut formatteritest
		web api oskab nii xml kui json kujul andmeid anda
		vaikimisi on XML, aga kui xml-formatter eemaldada, siis vaikimisi on json
		kui mõlemad on, saab juhtida, kumba kasutatakse
		lähemalt: https://www.asp.net/web-api/overview/formats-and-model-binding/json-and-xml-serialization?one=2

3. Et asi tööle hakkaks, 
	siis lisa palun parameetritele (seal api meetodites) [FromBody]

Televaatajate soovil lisasin väikese ajaxirakenduse Category/Detail lehele

1. lisasin sinna dropdowni, kus saab valida selle kategooria tooteid
	NB! Kuna ma mudelit ehitades lisasin tabelid eri aegadel,
	siis navigeerimine (Categry.Products) ei toimi
2. lisasin sinna mõned htmli elemendid, millel nimed
	dropdown - see lisatud dropdownlist
	nimetus - tablilahter, kuhu valitud toote nimetus pista
	hind - tabelilahter, kuhu valitud toote hind pista
3. lisasin dropdownile onchange funktsiooni (käivitatakse, kui valik muutub)
4. lisasin pisikese javascriptis tehtud funktsiooni(kese)
	$.ajax kolme parameetriga (objekt, mis koosneb "kolmest osast")
		* url, kuhu suunata (lisatakse lõppu valitud toote number)
		* funktsioon, mis täidetakse õnnestunud pöördumise korral
		* funktsioon, mis täidetakse ebaõnnestunud pöördumise korral
	õnnestumise korral paigutab loetud väljad neile mõeldud kohtadele lehel

	LISASIN VALIDEERIMISE

	Selleks lisasin paar klassi (vt Validation.cs)
	ning Employee klassis lisasin HireDatele ja BirthDatele mõned atribuudid

	Tegin selle kuupäevavalidaatori pisut ilusamaks

	Lisame rea testiks 1
	Lisame rea testiks 2