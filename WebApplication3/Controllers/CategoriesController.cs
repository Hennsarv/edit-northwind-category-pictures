﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication3;
using System.IO;
using Newtonsoft.Json;

namespace WebApplication3.Controllers
{
    public class CategoriesController : Controller
    {
        private NorthwindEntities db = new NorthwindEntities();

        public ActionResult CategoryImage(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Category c = db.Categories.Find(id.Value);
            if (c == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (c.Picture?.Length == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var picture = c.Picture.ToArray();
            if (picture[0] == 21) picture = picture.Skip(78).ToArray();
            return File(picture, "image/jpg");
        }

        public string CategoryName(int? id)
        {
            if (id == null) return "";
            Category c = db.Categories.Find(id.Value);
            if (c == null) return "";
            return c.CategoryName;

        }

        public string Category(int? id)
        {
            if (id == null) return null;
            Category c = db.Categories.Find(id.Value);
            if (c == null) return null;
            string s = JsonConvert.SerializeObject(new { c.CategoryID, c.CategoryName, c.Description });
            return s;
        }

        // GET: Categories
        public ActionResult Index(int? id = 0)
        {
            if (id > 0)
            ViewBag.Error = $"Selline kategooia on numbri all {id} juba olemas";
            return View(db.Categories.ToList());
        }

        // GET: Categories/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.Categories.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            ViewBag.Tooted = new SelectList(db.Products.Where(x => x.CategoryID == category.CategoryID).ToList(), "ProductID", "ProductName");

            Session["Viimatine"] = category;

            return View(category);
        }

        // GET: Categories/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Categories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CategoryID,CategoryName,Description,Picture")] Category category)
        {
            if (ModelState.IsValid)
            {
                var c = db.Categories.Where(x => x.CategoryName == category.CategoryName).FirstOrDefault();
                if (c != null)    return RedirectToAction("Index", new { id = c.CategoryID });
                db.Categories.Add(category);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(category);
        }

        // GET: Categories/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.Categories.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        // POST: Categories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CategoryID,CategoryName,Description")] Category category, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                db.Entry(category).State = EntityState.Modified;
                db.Entry(category).Property(x => x.Picture).IsModified = false;
                if (file != null && file.ContentLength > 0)
                {
                    using (BinaryReader br = new BinaryReader(file.InputStream))
                    {
                        byte[] buff = br.ReadBytes(file.ContentLength);
                        category.Picture = buff;
                    }
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(category);
        }

        // GET: Categories/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.Categories.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        // POST: Categories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Category category = db.Categories.Find(id);
            db.Categories.Remove(category);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
