﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication3.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Response.Write("aga siin");

            if (Session["Viimatine"] != null)
            {
                Category c = Session["Viimatine"] as Category;
                ViewBag.Viimatine = c.CategoryName;
            }
            else ViewBag.Viimatine = "categooriat pole veel kaenud keegi";
            return View("Index");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return Index();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}