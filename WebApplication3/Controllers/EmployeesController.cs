﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication3;
using System.IO;

namespace WebApplication3
{
    partial class Employee
    {
        public string FullName => FirstName + " " + LastName;
    }
}

namespace WebApplication3.Controllers
{
    public class EmployeesController : Controller
    {
        private NorthwindEntities db = new NorthwindEntities();

        public ActionResult Photo(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Employee e = db.Employees.Find(id.Value);
            if (e == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (e.Photo?.Length == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var picture = e.Photo.ToArray();
            if (picture[0] == 21) picture = picture.Skip(78).ToArray();
            return File(picture, "image/jpg");
        }
        // GET: Employees
        public ViewResult Index()
        {
            var employees = db.Employees.Include(e => e.Employee1);
            return View(employees.ToList());
        }

        // GET: Employees/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                
            }
            Employee employee = db.Employees.Include(x => x.Employees1).Where(x => x.EmployeeID == (id??0)).SingleOrDefault();
            if (employee == null)
            {
                return HttpNotFound();
            }
            ViewBag.Alluvad = employee.Employees1.ToList();
            return View(employee);
        }

        // GET: Employees/Create
        public ActionResult Create(int reportsTo = 0)
        {
            ViewBag.ManagerID = reportsTo;
            Employee employee = new Employee { ReportsTo = reportsTo };
            ViewBag.ReportsTo = new SelectList(db.Employees, "EmployeeID", "LastName");
            return View(employee);
        }

        // POST: Employees/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EmployeeID,LastName,FirstName,Title,TitleOfCourtesy,BirthDate,HireDate,Address,City,Region,PostalCode,Country,HomePhone,Extension,Photo,Notes,ReportsTo,PhotoPath")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                db.Employees.Add(employee);
                db.SaveChanges();
                //                return RedirectToAction("Index");
                return RedirectToAction("Details", new { id = employee.ReportsTo });
            }

            ViewBag.ReportsTo = new SelectList(db.Employees, "EmployeeID", "LastName", employee.ReportsTo);
            return View(employee);
        }

        // GET: Employees/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            ViewBag.ReportsTo = new SelectList(db.Employees, "EmployeeID", "LastName", employee.ReportsTo);
            return View(employee);
        }

        // POST: Employees/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EmployeeID,LastName,FirstName,Title,TitleOfCourtesy,BirthDate,HireDate,Address,City,Region,PostalCode,Country,HomePhone,Extension,Notes,ReportsTo,Photo,PhotoPath")] Employee employee, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                // - tundub tobe -- employee.Photo = db.Employees.Find(employee.EmployeeID).Photo;
                db.Entry(employee).State = EntityState.Modified;
                db.Entry(employee).Property(x => x.Photo).IsModified = false;
                if (file != null && file.ContentLength > 0)
                {
                    
                    using (BinaryReader br = new BinaryReader(file.InputStream))
                    {
                        byte[] buff = br.ReadBytes(file.ContentLength);
                        employee.Photo = buff;
                    }
                }
                if (employee.FirstName == "Henn")
                {
                    ViewBag.ErrorMessage = "Hennusid on maailmas vaid 1 - vali teine nimi";
                    ViewBag.ReportsTo = new SelectList(db.Employees, "EmployeeID", "LastName", employee.ReportsTo);

                    return View(employee);
                }

                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ReportsTo = new SelectList(db.Employees, "EmployeeID", "LastName", employee.ReportsTo);
            

            return View(employee);
        }

        // GET: Employees/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: Employees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Employee employee = db.Employees.Find(id);
            db.Employees.Remove(employee);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
